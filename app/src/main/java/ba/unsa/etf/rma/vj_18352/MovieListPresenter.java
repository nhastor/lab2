package ba.unsa.etf.rma.vj_18352;

import android.content.Context;

public class MovieListPresenter implements ListPresenter {
    private MovieListInteractor movieListInteractor;
    private MainActivity movieListView;
    private Context context;

    public MovieListPresenter(MainActivity movieListView) {
        this.movieListInteractor = new MovieListInteractor();
        this.movieListView = movieListView;
        this.context = movieListView;
        refreshMovies();
    }

    public void refreshMovies(){
        movieListView.setMovies(movieListInteractor.get());
    }
}
