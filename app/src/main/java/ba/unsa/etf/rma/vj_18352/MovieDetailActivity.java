package ba.unsa.etf.rma.vj_18352;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MovieDetailActivity extends AppCompatActivity {
    private Movie movie;

    public MovieDetailActivity() {
        movie = new Movie("Se7en", "Thriller", "1995", "https://www.imdb.com/title/tt0114369/", "Najbolji film");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_detail_list);

        ((TextView) findViewById(R.id.textViewTitle)).setText((CharSequence) "Title:");
        ((TextView) findViewById(R.id.textViewGenre)).setText((CharSequence) "Genre:");
        ((TextView) findViewById(R.id.textViewReleaseDate)).setText((CharSequence) "Release date:");
        ((TextView) findViewById(R.id.textViewHomePage)).setText((CharSequence) "Home page:");
        ((TextView) findViewById(R.id.textViewOverview)).setText((CharSequence) "Overview:");
        ((TextView) findViewById(R.id.textViewActors)).setText((CharSequence) "Actors:");

        ((TextView) findViewById(R.id.textViewMovieTitle)).setText((CharSequence) movie.getTitle());
        ((TextView) findViewById(R.id.textViewMovieGenre)).setText((CharSequence) movie.getGenre());
        ((TextView) findViewById(R.id.textViewMovieReleaseDate)).setText((CharSequence) movie.getReleaseDate());
        ((TextView) findViewById(R.id.textViewMovieHomePage)).setText((CharSequence) movie.getHomepage());
        ((TextView) findViewById(R.id.textViewMovieOverview)).setText((CharSequence) movie.getOverview());

        ((TextView) findViewById(R.id.textViewActor1)).setText((CharSequence) "Actor1");
        ((TextView) findViewById(R.id.textViewActor2)).setText((CharSequence) "Actor2");
        ((TextView) findViewById(R.id.textViewActor3)).setText((CharSequence) "Actor3");
        ((TextView) findViewById(R.id.textViewActor4)).setText((CharSequence) "Actor4");
        ((TextView) findViewById(R.id.textViewActor5)).setText((CharSequence) "Actor5");


    }

    }
