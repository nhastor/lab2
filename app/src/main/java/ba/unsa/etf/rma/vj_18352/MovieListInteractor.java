package ba.unsa.etf.rma.vj_18352;

import java.util.List;

public class MovieListInteractor implements ListInteractor{
    public List<Movie> get(){
        return MoviesModel.getMovies();
    }
}
