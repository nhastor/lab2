package ba.unsa.etf.rma.vj_18352;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity{

    private Button button;
    private EditText editText;
    private ListView listView;

    private List<Movie> entries;
    private MovieListAdapter movieListAdapter;
    private MovieListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button)findViewById(R.id.button);
        editText = (EditText)findViewById(R.id.editText);
        listView = (ListView)findViewById(R.id.listView);

        entries = MoviesModel.getMovies();
        movieListAdapter = new MovieListAdapter(this, R.layout.list_element, entries);
        presenter = new MovieListPresenter(this);


        listView.setAdapter(movieListAdapter);

        listView.setOnItemClickListener(listItemClickListener);

//        button.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                entries.add(editText.getText().toString());
//                adapter.notifyDataSetChanged();
//                editText.setText("");
//            }
//        });
    }

    public void setMovies(List<Movie> movies){
        entries = movies;
        notifyMovieListDataSetChanged();
    }

    public void notifyMovieListDataSetChanged(){
        movieListAdapter.notifyDataSetChanged();
    }

    private AdapterView.OnItemClickListener listItemClickListener =
            new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    Intent movieDetailIntent = new Intent(MainActivity.this,
                            MovieDetailActivity.class);
                    Movie movie = movieListAdapter.getMovie(position);
                    movieDetailIntent.putExtra("title", movie.getTitle());
                    MainActivity.this.startActivity(movieDetailIntent);
                }
            };
}
